# scalaStudy

## 1. 介绍

scala编程实践
记录在开发过程中使用scala语言实现的相关功能

## 2. 软件运行环境
scala 2.12.10

## 3. 说明

### 3.1 Io文件夹

#### **3.1.1FileToArrayByte**

文件转字节数组的多种方法

#### 3.1.2 ScalaIo

常用java Io方法改用scala编写

------

### 3.2 Util文件夹

#### 3.2.1 ArrayByteUtil

scala操作Array[Byte]

#### 3.2.2 HadoopUtil

scala操作hadoop

#### 3.2.3 PropertyUtil

scala操作配置文件

------

### 3.3 hadoop文件夹

#### 3.3.1 HadoopApi

scala操作hadoop集群

------

### 3.4 remote文件夹

spark自定义Rdd,通过j2ssh读取远程文件,按照指定schema加载

### 3.5 mannual文件夹

hadoop+zookeeper+kafka+spark集群搭建手册

nfs服务搭建手册

存储分类笔记
