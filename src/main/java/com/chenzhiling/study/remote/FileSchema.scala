package com.chenzhiling.study.remote

import com.chenzhiling.study.util.PropertiesUtil.getString
import org.apache.spark.sql.types._

import scala.collection.mutable.ArrayBuffer

/**
 * @Author: CHEN ZHI LING
 * @Date: 2021/8/11
 * @Description: 自定义文件schema
 */
object FileSchema {

  private val SPLIT_NUMBER: Int = getString("file.split.number").toInt
  private val FILE_NAME = "fileName"
  private val PATH = "path"
  private val SUFFIX = "suffix"
  private val SIZE = "size"
  private val COMMON_FILE = "commonFile"
  private val BYTE_ARRAY = "byteArray"
  val schema: StructType = StructType(
    StructField(FILE_NAME, StringType, nullable = true) ::
      StructField(PATH, StringType, nullable = true) ::
      StructField(SUFFIX, StringType, nullable = true) ::
      StructField(SIZE, LongType, nullable = true) ::
      StructField(COMMON_FILE,getSubSchema):: Nil)



  def getSubSchema: StructType={
    val fields = new ArrayBuffer[StructField]
    //根据切分份数，生成n个字段
    for (i <- 1 to SPLIT_NUMBER){
      fields.append(StructField(new StringBuilder(BYTE_ARRAY+i).toString(), BinaryType, nullable = false))
    }
    StructType(fields.toArray)
  }

}
