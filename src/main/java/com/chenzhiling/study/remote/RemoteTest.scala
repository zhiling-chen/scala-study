package com.chenzhiling.study.remote

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

/**
 * @Author: CHEN ZHI LING
 * @Date: 2021/8/11
 * @Description: 远程文件读取测试累
 */
object RemoteTest {


  def main(args: Array[String]): Unit = {
    val ip = "ip"
    val port = 22
    val username = "用户名"
    val password = "密码"
    val path = "路径"
    val spark: SparkSession = SparkSession.builder().appName("RemoteTest").master("local").getOrCreate()
    val rdd = new OnlineRdd(spark.sparkContext, ip, port, username, password, path)
    val rows: Array[Row] = rdd.collect()
    val value: RDD[Row] = spark.sparkContext.parallelize(rows)
    val frame: DataFrame = spark.sqlContext.createDataFrame(value, FileSchema.schema)
    frame.show()
  }

}
