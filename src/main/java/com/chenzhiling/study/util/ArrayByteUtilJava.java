package com.chenzhiling.study.util;

import scala.util.control.Exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: CHEN ZHI LING
 * @Date: 2021/7/8
 * @Description: 字节数组切分
 */
public class ArrayByteUtilJava {


    public static List<byte[]> chunkArray(byte [] array, int chunkSize) {
        int numOfChunks = (int)Math.ceil((double)array.length / chunkSize);
        List<byte[]> list = new ArrayList<>();
        for(int i = 0; i < numOfChunks; ++i) {
            int start = i * chunkSize;
            int length = Math.min(array.length - start, chunkSize);
            byte[] subArray = new byte[length];
            System.arraycopy(array, start, subArray, 0, length);
            list.add(subArray);
        }

        return list;
    }


    public static void main(String[] args) {
        byte[] bt = new byte[11];
        for (int i = 0; i < 11; i++) {
            bt[i] = (byte) i;
        }
        List<byte[]> lists = chunkArray(bt, 9);
        for (byte[] list : lists) {
            System.out.println(Arrays.toString(list));
        }

    }


}
