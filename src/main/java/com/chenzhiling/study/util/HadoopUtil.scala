package com.chenzhiling.study.util

import org.apache.hadoop.fs.{FileStatus, FileSystem, Path}

/**
 * @Author: CHEN ZHI LING
 * @Date: 2021/7/30
 * @Description:
 */
object HadoopUtil {

  /**
   * 列出指定路径下的所有文件
   * @param path 路径
   * @param fs hadoop fileSystem
   * @return
   */
  def listFiles(path: Path,fs:FileSystem): Array[String] = {
    fs.listStatus(path).filter((_:FileStatus).isFile).map((_: FileStatus).getPath.toString) ++
      fs.listStatus(path).filter((_:FileStatus).isDirectory).flatMap((subPath: FileStatus) => listFiles(subPath.getPath,fs))
  }
}
