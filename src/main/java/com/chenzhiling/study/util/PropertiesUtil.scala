package com.chenzhiling.study.util

import java.io.{BufferedInputStream, FileInputStream}
import java.util.Properties

/**
 * @Author: CHEN ZHI LING
 * @Date: 2021/7/9
 * @Description: scala读取配置文件
 */
object PropertiesUtil extends Serializable {


  val properties: Properties = getProperties("/src/main/resources/scala.properties")


  def getProperties(path:String):Properties={
    val filePath: String = System.getProperty("user.dir")
    val prop = new Properties
    val ipStream = new BufferedInputStream(new FileInputStream(filePath + path))
    prop.load(ipStream)
    prop
  }

  def getString(path:String):String={
    properties.getProperty(path)
  }


  def main(args: Array[String]): Unit = {
    println(getString("test"))
  }

}
